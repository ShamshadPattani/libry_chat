package com.ktulibary.devlop.bigone.examplktu.Libaray;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.ktulibary.devlop.bigone.examplktu.R;
import com.ktulibary.devlop.bigone.examplktu.Upload;

import java.util.ArrayList;
import java.util.Objects;

public class SubjectView extends AppCompatActivity {
    private  String reciveID,sem;
    RecyclerView recyclerView;
    Button upload;


    FirebaseDatabase database;
    FirebaseStorage storage;
    Uri pdfuri;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_view2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.Regtool);
        setSupportActionBar(toolbar);
        recyclerView=findViewById(R.id.subjectViewrecycler);
    //    upload=findViewById(R.id.upload1);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final MyAdapter myAdapter=new MyAdapter(recyclerView,SubjectView.this,new ArrayList<String>(),new ArrayList<String>());
        recyclerView.setAdapter(myAdapter);
        database=FirebaseDatabase.getInstance();
        storage=FirebaseStorage.getInstance();
        //for download


        sem= Objects.requireNonNull(getIntent().getExtras().get("semester_name")).toString();
        // LocdatabaseReference= FirebaseDatabase.getInstance().getReference().child(sem);
        reciveID=getIntent().getExtras().get("sub_name").toString();

        //for  set Toolbar name
     DatabaseReference ref=database.getReference();
     DatabaseReference subname=ref.child(sem).child(reciveID).child("sub");
     subname.addValueEventListener(new ValueEventListener() {
         @Override
         public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
             String toolbarname=dataSnapshot.getValue(String.class);
             Toast.makeText(SubjectView.this,"ID:"+toolbarname,Toast.LENGTH_SHORT).show();
             getSupportActionBar().setTitle(toolbarname);
         }

         @Override
         public void onCancelled(@NonNull DatabaseError databaseError) {

         }
     });

       Toast.makeText(this,"ID:"+reciveID,Toast.LENGTH_SHORT).show();
        // RetriveSUBJECTS();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           Intent intent =new Intent(SubjectView.this,Upload.class);
          intent.putExtra("id",reciveID);
          intent.putExtra("semester_name",sem);
           startActivity(intent);
            }
        });

        //Download... and see
  DatabaseReference pdfview=ref.child("PDF").child(reciveID);
pdfview.addChildEventListener(new ChildEventListener() {
    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        String fileName=dataSnapshot.getKey();
        String url=dataSnapshot.getValue(String.class);
        ((MyAdapter)recyclerView.getAdapter()).update(fileName,url);
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
});
    }
}
