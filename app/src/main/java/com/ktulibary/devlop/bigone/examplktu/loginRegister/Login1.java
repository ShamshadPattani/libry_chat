package com.ktulibary.devlop.bigone.examplktu.loginRegister;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ktulibary.devlop.bigone.examplktu.R;
import com.ktulibary.devlop.bigone.examplktu.Libaray.home;

public class Login1 extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    Button singUp,signIn;
    TextView forgot;
    EditText emil,passwrd;
   LinearLayout myLayout;
    AnimationDrawable animationDrawable;

    private ProgressDialog LoadingBar;
    private FirebaseAuth mAuth;
    private int myLovelyVariable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);

        mAuth = FirebaseAuth.getInstance();

        forgot=findViewById(R.id.forgotlink);
        singUp=findViewById(R.id.registr);
        signIn=findViewById(R.id.Signin);
        emil=findViewById(R.id.txtemail);
        passwrd=findViewById(R.id.passwrd_txt);

        LoadingBar=new ProgressDialog(this);
        signIn.setOnClickListener(this);
       singUp.setOnClickListener(this);
        forgot.setOnClickListener(this);
  // for animation in login page
        myLayout = findViewById(R.id.myLayout);
        animationDrawable = (AnimationDrawable) myLayout.getBackground();
        animationDrawable.setEnterFadeDuration(4500);
        animationDrawable.setExitFadeDuration(4500);
        animationDrawable.start();

    }

    @Override
    public void onClick(View v) {
        if(v==singUp){
            Intent signIntent = new Intent(Login1.this, RegisterActvity.class);
            startActivity(signIntent);
        }else if(v==signIn){
            String eml=emil.getText().toString();
            String pwd=passwrd.getText().toString();

            if(eml.length()==0)
            {
                emil.setError("Missing");
            }
            else if(pwd.length()==0)
            {
                passwrd.setError("Missing");
            }
            else {
                LoadingBar.setTitle("Loading...");
                LoadingBar.setMessage("please wait,,,,");
                LoadingBar.show();
                 mAuth.signInWithEmailAndPassword(eml,pwd)
                         .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                             @Override
                             public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){
                                        Intent loginINTENT = new Intent(Login1.this, home.class);
                                        loginINTENT.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(loginINTENT);
                                    }else{
                                        Toast.makeText(Login1.this,"Error!please check your email and password",Toast.LENGTH_LONG).show();
                                    }

                                 LoadingBar.dismiss();
                             }
                         });

            }

        }else if(v==forgot)
          {
              Intent fortgotINTENT = new Intent(Login1.this, forgotPassword.class);
              startActivity(fortgotINTENT);
        }
    }
}
