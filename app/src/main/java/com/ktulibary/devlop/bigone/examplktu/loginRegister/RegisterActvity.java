package com.ktulibary.devlop.bigone.examplktu.loginRegister;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ktulibary.devlop.bigone.examplktu.R;

import java.util.ArrayList;
import java.util.Objects;




public class RegisterActvity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private Toolbar mToolBar;
    private RadioGroup Radiogrp;
    private RadioButton mradioButton;
    EditText RegName, RegEmail, RegPass,RegcofigPass;
    Button CreateAccButton;
    RadioButton rb, rb2;
    TextView mitemText;
    TextView link_log;
    String[] listItem;
    String Status=" ",subjetname,selectedtext;
    boolean[] CheckListItem;
    //for saving subjcts in array
    ArrayList<Integer> selectsubject = new ArrayList<>();
    //fire base
    private FirebaseAuth mAuth;
    private DatabaseReference regdatabaseReference;
    private ProgressDialog LoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        //toolbar
        mToolBar = findViewById(R.id.register_Toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        //Button
        CreateAccButton = findViewById(R.id.btn_signup);
        CreateAccButton.setOnClickListener(this);
        //link for already a member
        link_log=findViewById(R.id.link_login);
        link_log.setOnClickListener(this);


        //initizalition
        mitemText = findViewById(R.id.select);
        rb = findViewById(R.id.teacherRadio);
        rb2 = findViewById(R.id.studentRadio);
        LoadingBar=new ProgressDialog(this);
        RegName = findViewById(R.id.input_name);
        RegEmail = findViewById(R.id.input_email);
        RegPass = findViewById(R.id.input_password);
        RegcofigPass=findViewById(R.id.confirm_password);
        Radiogrp = findViewById(R.id.radiogrp);
//
        selectedtext = (String) rb.getText();
        //for popup window
        listItem = getResources().getStringArray(R.array.RegisterTeacher);
        CheckListItem = new boolean[listItem.length];
        //setonclick
        rb.setOnClickListener(this);
        rb2.setOnClickListener(this);
        //Setonchecked
        Radiogrp.setOnCheckedChangeListener(this);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {

//radiobutton of teacher clicked
        if (v == rb) {
            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActvity.this);
            builder.setTitle("Select your subjects:");
            builder.setMultiChoiceItems(listItem, CheckListItem, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int position, boolean isChecked) {
                    if (isChecked) {
                        if (!selectsubject.contains(position)) {
                            selectsubject.add(position);
                        } else {
                            selectsubject.remove(position);
                        }
                    }
                }
            });
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String item ="";
                    for (int i = 0; i < selectsubject.size(); i++) {
                        item=item+listItem[selectsubject.get(i)];
                        if (i != selectsubject.size() - 1) {
                            item= item + ",";
                        }
                    }
                    mitemText.setText(item);
                    subjetname=mitemText.getText().toString();
                }
            });
            builder.setNeutralButton("Clear all", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    for (int i = 0; i < CheckListItem.length; i++) {
                        CheckListItem[i] = false;
                        selectsubject.clear();
                        mitemText.setText("");
                    }
                }
            });
            AlertDialog mdialog = builder.create();
            mdialog.show();

            //create button
        } else if (v==rb2)
        {
            mitemText.setText("");
            subjetname=null;
        }
        else if (v == CreateAccButton)
        {
            final String name = RegName.getText().toString();
            final String email = RegEmail.getText().toString();
            String password = RegPass.getText().toString();
            String confrmpsw=RegcofigPass.getText().toString();
            if(name.length()==0)
            {
                RegName.setError("Missing");
            }
            else if(email.length()==0)
            {
                RegEmail.setError("Missing");
            }
            else if(password.length()==0)
            {
                RegPass.setError("Missing");
            }
            else if(confrmpsw.length()==0)
            {
                RegcofigPass.setError("Missing");
            }
            else if(!confrmpsw.equals(password))
            {
                RegcofigPass.setError("Mismatch Password");
            }else if(Radiogrp.getCheckedRadioButtonId()==-1){
                Toast.makeText(getApplicationContext(),"Select your status",Toast.LENGTH_LONG).show();
            }else if(password.length()<6){
                Toast.makeText(getApplicationContext(),"password should be greater than 6 digits",Toast.LENGTH_LONG).show();
            }
            else {
                //code for firebase db
                LoadingBar.setTitle("Loading");
                LoadingBar.setMessage("please wait....");
                LoadingBar.show();
                mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            String curent_user_id = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                            regdatabaseReference = FirebaseDatabase.getInstance().getReference().child("user").child(curent_user_id);
                            regdatabaseReference.child("user_name").setValue(name);
                            regdatabaseReference.child("user_email").setValue(email);
                            regdatabaseReference.child("subjects").setValue(subjetname);
                            regdatabaseReference.child("status").setValue(Status).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Intent signIntent = new Intent(RegisterActvity.this, Login1.class);
                                        startActivity(signIntent);
                                    }else{
                                        Toast.makeText(getApplicationContext(),"Error!",Toast.LENGTH_LONG).show();
                                    }
                                    LoadingBar.dismiss();
                                }
                            });
                        }
                    }
                });
            }
        }
        else if (v==link_log)
        {
            Intent LinkIntent = new Intent(RegisterActvity.this, Login1.class);
            startActivity(LinkIntent);
        }

    }








    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (group == Radiogrp) {
            mradioButton = Radiogrp.findViewById(checkedId);

            switch (checkedId) {
                case R.id.teacherRadio:
                    Status = "Teacher";
                    break;
                case R.id.studentRadio:
                    Status = "Student";
                    break;
                default:

            }
        }
    }
}


