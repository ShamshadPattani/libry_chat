package com.ktulibary.devlop.bigone.examplktu;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class chatRecycleView extends RecyclerView.Adapter<chatRecycleView.msgViewHolder> {
    private List<chatDataList> items;
    private Context mcontext;
    private final int YOU = 1, ME = 2;

    //constructor
    public chatRecycleView(Chat chat, List<chatDataList> items) {

        this.items = items;
    }

    @NonNull
    @Override
    public msgViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.chat,parent,false);
        return new msgViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull msgViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class msgViewHolder extends RecyclerView.ViewHolder{
           public TextView sendtxet,sendrtime,recivetxt,recivetime;
       public msgViewHolder(View itemView) {
           super(itemView);
           sendtxet=itemView.findViewById(R.id.sendtxt);
           sendrtime=itemView.findViewById(R.id.sendtime);
           recivetime=itemView.findViewById(R.id.recivetime);
           recivetxt=itemView.findViewById(R.id.recivetxt);
       }
   }
}
