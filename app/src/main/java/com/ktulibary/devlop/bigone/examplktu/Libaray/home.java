package com.ktulibary.devlop.bigone.examplktu.Libaray;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.ktulibary.devlop.bigone.examplktu.Chat;
import com.ktulibary.devlop.bigone.examplktu.R;
import com.ktulibary.devlop.bigone.examplktu.loginRegister.Login1;
import com.ktulibary.devlop.bigone.examplktu.profile;

public class home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private TextView mTextMessage;
    String arrayname[] = {"S1",
            "S2",
            "S3",
            "S4",
            "S5",
            "S6"};
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //Toolbar for Home
        mToolbar =findViewById(R.id.home_appbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Library Home");
        //NAVIGATION DRAWER
        mDrawerLayout= findViewById(R.id.home_page);
        mToggle=new ActionBarDrawerToggle(home.this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //For Selecting Navigation Item Selection
        navigationView=findViewById(R.id.nav_view);
        View navView=navigationView.inflateHeaderView(R.layout.nav_header);
        navigationView.setNavigationItemSelectedListener(this);
        //BOTTOM NAVIGATION
        BottomNavigationView bottomNavigationView=findViewById(R.id.navigation);
        Menu menu =bottomNavigationView.getMenu();
        MenuItem item=menu.getItem(0);
        item.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_lib:
                        Intent i = new Intent(getApplicationContext(),home.class);
                        startActivity(i);
                        break;
                    case R.id.navigation_chat:
                        Intent j = new Intent(getApplicationContext(),Chat.class);
                        startActivity(j);
                        break;
                    case R.id.navigation_forum:
                        Intent k = new Intent(getApplicationContext(),Login1.class);
                        startActivity(k);
                        break;

                }
                return false;
            }
        });
//circle MCA menu and animation code
        CircleMenu circleMenu = (CircleMenu) findViewById(R.id.circleMenu);
        circleMenu.setMainMenu(Color.parseColor("#fcb90f9f"), R.drawable.mca, R.drawable.mca)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s1)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s2)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s3)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s4)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s5)
                .addSubMenu(Color.parseColor("#FF058AEF"), R.drawable.s6)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {
                    @Override
                    public void onMenuSelected(int index) {
                        //Toast.makeText(home.this, "a=" + arrayname[0], Toast.LENGTH_LONG).show();
                        if (arrayname[index] == "S1") {
                            Intent intent = new Intent(home.this, Semster.class);
                            intent.putExtra("semester","s1");
                            startActivity(intent);
                        } else if (arrayname[index] == "S2") {
                            Intent intent = new Intent(home.this, Semster.class);
                            intent.putExtra("semester","s2");
                            startActivity(intent);
                        } else if (arrayname[index] == "S3") {
                            Intent intent = new Intent(home.this, Semster.class);
                            intent.putExtra("semester","s3");
                            startActivity(intent);
                        } else if (arrayname[index] == "S4") {
                            Intent intent = new Intent(home.this, Semster.class);
                            intent.putExtra("semester","s4");
                            startActivity(intent);
                        } else if (arrayname[index] == "S5") {
                            Intent intent = new Intent(home.this, Semster.class);
                            intent.putExtra("semester","s5");
                            startActivity(intent);
                        } else if (arrayname[index] == "S6") {
                            Intent intent = new Intent(home.this, S6.class);
                            startActivity(intent);
                        }
                    }

                });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.profile)
        {
            Intent intent = new Intent(home.this, profile.class);
            startActivity(intent);
        }
        else if (id==R.id.settings)
        {
            Intent intent = new Intent(home.this, profile.class);
            startActivity(intent);
        }
        else if (id==R.id.share)
        {
            Intent intent = new Intent(home.this, profile.class);
            startActivity(intent);
        }
        else if (id==R.id.about)
        {
            Intent intent = new Intent(home.this, profile.class);
            startActivity(intent);
        }
        else if (id==R.id.logout)
        {
            Intent intent = new Intent(home.this, profile.class);
            startActivity(intent);
        }
        return false;
    }

}
