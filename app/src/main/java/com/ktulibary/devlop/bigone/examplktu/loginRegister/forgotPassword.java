package com.ktulibary.devlop.bigone.examplktu.loginRegister;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.ktulibary.devlop.bigone.examplktu.R;

public class forgotPassword extends AppCompatActivity {
    private Button resetButton;
    private EditText resetemailtxt;
    private FirebaseAuth mauth;
    private Toolbar mToolbar;

    private ProgressDialog LoadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mauth=FirebaseAuth.getInstance();

        mToolbar=findViewById(R.id.Forgot_Toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LoadingBar=new ProgressDialog(this);
        resetButton=findViewById(R.id.forgotpassButton);
        resetemailtxt=findViewById(R.id.field_email);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=resetemailtxt.getText().toString();
                if(email.length()==0)
                {
                    resetemailtxt.setError("missing");
                }else{
                    LoadingBar.setTitle("Loading...");
                    LoadingBar.setMessage("please wait,");
                    LoadingBar.show();
                    mauth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(forgotPassword.this,"Please check your email",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(forgotPassword.this,Login1.class));
                            }else{
                                String msg=task.getException().toString();
                                Toast.makeText(forgotPassword.this,"Error occured"+msg,Toast.LENGTH_LONG).show();
                            }
                            LoadingBar.dismiss();
                        }
                    });
                }
            }
        });
    }
}
