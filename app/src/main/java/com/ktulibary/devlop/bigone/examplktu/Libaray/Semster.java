package com.ktulibary.devlop.bigone.examplktu.Libaray;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ktulibary.devlop.bigone.examplktu.R;

import java.util.Objects;

public class Semster extends AppCompatActivity {
    private android.support.v7.widget.Toolbar mtoolbar;
    private RecyclerView mrecyclerView;
    private DatabaseReference subjectRefrence;

    // to read the semester values(s1,s2,s3,s4,s5..)
    String semester;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semster);
        mtoolbar = findViewById(R.id.Regtool);
        setSupportActionBar(mtoolbar);

        semester = getIntent().getStringExtra("semester");
        Objects.requireNonNull(getSupportActionBar()).setTitle(semester);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mrecyclerView = (RecyclerView) findViewById(R.id.recyclerSUb);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(this));
        subjectRefrence = FirebaseDatabase.getInstance().getReference().child(semester);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<subjects> options=
                new FirebaseRecyclerOptions.Builder<subjects>()
                        .setQuery(subjectRefrence,subjects.class)
                        .build();

        FirebaseRecyclerAdapter<subjects,SYLBViewHoldere> adapter=
                new FirebaseRecyclerAdapter<subjects, SYLBViewHoldere>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull SYLBViewHoldere holder, final int position, @NonNull subjects model) {

                        holder.t1.setText(model.getSub());
                        //click on each item view(click each subject go to subjectView)
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                               //get clicked item key
                                String sub_name=getRef(position).getKey();
                                Intent subintent=new Intent(Semster.this,SubjectView.class);
                                subintent.putExtra("sub_name",sub_name);
                                //to pass  semester(s1,s2,s3,s4,s5) based on the click
                                subintent.putExtra("semester_name",semester);
                                startActivity(subintent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public SYLBViewHoldere onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.all_sub,parent,false);
                        SYLBViewHoldere viewHoldere=new SYLBViewHoldere(view);
                        return viewHoldere;
                    }
                };
        mrecyclerView.setAdapter(adapter);
        adapter.startListening();
    }
    public  class SYLBViewHoldere extends RecyclerView.ViewHolder {
        TextView t1,t2;

        public SYLBViewHoldere(View itemView) {
            super(itemView);
            t1=itemView.findViewById(R.id.t1);
            //  t2=itemView.findViewById(R.id.t2);
        }
    }
}
