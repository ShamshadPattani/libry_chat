package com.ktulibary.devlop.bigone.examplktu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ktulibary.devlop.bigone.examplktu.Libaray.Semster;
import com.ktulibary.devlop.bigone.examplktu.loginRegister.Login1;
import com.ktulibary.devlop.bigone.examplktu.loginRegister.RegisterActvity;

import java.util.Objects;

public class Upload extends AppCompatActivity {
    Button upldbutton;
    EditText name;
    String filename,editname,filename1;
    private String id, sem;
    public static final int REQST_CODE = 1234;
    FirebaseDatabase database;
    FirebaseStorage storage;
    Uri pdfuri;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        upldbutton = findViewById(R.id.UploadButton);
        name = findViewById(R.id.uploadEdit);

        //Toast.makeText(getApplicationContext(),qeditname,Toast.LENGTH_LONG).show();
       filename = System.currentTimeMillis() + ".pdf";
       int random=(int)(Math.random()*900)+1000;
        filename1=""+random;
        database = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        id = Objects.requireNonNull(getIntent().getExtras().get("id")).toString();
        sem= Objects.requireNonNull(getIntent().getExtras().get("semester_name")).toString();
        //for upload..
        upldbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String editname = name.getText().toString();
                if (editname.length() == 0) {
                    name.setError("Missing");
                }else {
                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);//fetch files
                    startActivityForResult(intent, REQST_CODE);
                }
            }
        });
    }
    //this function will get the pdf from the storage
    private void getPDF() {
        //for greater than lolipop versions we need the permissions asked on runtime
        //so if the permission is not available user will go to the screen to allow storage permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            //if a file is selected
            if (data.getData() != null) {
                //uploading the file
                uploadFile1(data.getData());
            } else {
                Toast.makeText(this, "No file chosen", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadFile1(Uri data) {
        progressDialog = new ProgressDialog(Upload.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("uploading.....");
        progressDialog.setProgress(0);
        progressDialog.show();
        Uri selectedImageUri = data;

        final StorageReference storageRef = storage.getReference();
        final StorageReference ref = storageRef.child("uploads").child(id).child(filename);
        Task<UploadTask.TaskSnapshot> uploadTask = ref.putFile(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                        saveUrl(downloadUri.toString());
                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }

    private void saveUrl(String s) {

        database.getReference().child("PDF").child(id).child(filename1+name.getText().toString())
                .setValue(s)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            progressDialog.setProgress(100);
                            progressDialog.dismiss();
                            Toast.makeText(Upload.this, "Update Success fully ", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Upload.this, "Error", Toast.LENGTH_LONG).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Upload.this, "Error in upload", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void uploadFile(final Uri data) {
        progressDialog = new ProgressDialog(Upload.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setTitle("uploading.....");
        progressDialog.setProgress(0);
        progressDialog.show();
       // mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        final StorageReference storageReference = storage.getReference();
        storageReference.child("uploads").child(id).child(filename).putFile(data)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       // Task<Uri> downloadUrl = storageReference.getDownloadUrl();
                      final String url = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                        //store the data to real time DB
                        DatabaseReference reference = database.getReference();
                        final String pdfname = name.getText().toString();
                        final String  qeditname=name.getText().toString();
                       // reference.child("PDF").child(id).child(filename1+"name").setValue(pdfname);
                        reference.child("PDF").child(id).child(filename1+qeditname).setValue(url).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                   progressDialog.dismiss();
                                    Toast.makeText(Upload.this, "Su", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(Upload.this, "Error", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Upload.this, "Error in upload", Toast.LENGTH_LONG).show();
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                //progress bar
                int curntpogress = (int) (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                progressDialog.setProgress(curntpogress);
            }
        });
    }
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.UploadButton:
                getPDF();
                break;
        }
    }


}