package com.ktulibary.devlop.bigone.examplktu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;

import com.ktulibary.devlop.bigone.examplktu.Libaray.home;
import com.ktulibary.devlop.bigone.examplktu.loginRegister.Login1;

public class Chat extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private chatRecycleView mAdapter;
    private EditText txt;
    private ImageButton send;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_of_chat);
        //BOTTOM NAVIGATION
        BottomNavigationView bottomNavigationView=findViewById(R.id.navigation);
        Menu menu =bottomNavigationView.getMenu();
        MenuItem item=menu.getItem(1);
        item.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_lib:
                        Intent i = new Intent(getApplicationContext(), home.class);
                        startActivity(i);
                        break;
                    case R.id.navigation_chat:
                        Intent j = new Intent(getApplicationContext(),Chat.class);
                        startActivity(j);
                        break;
                    case R.id.navigation_forum:
                        Intent k = new Intent(getApplicationContext(),Login1.class);
                        startActivity(k);
                        break;

                }
                return false;
            }
        });

    }


}