package com.ktulibary.devlop.bigone.examplktu;

public class chatDataList {
String text,time,date,from,type;

public chatDataList(){}

    public chatDataList(String text, String time, String date, String from, String type) {
        this.text = text;
        this.time = time;
        this.date = date;
        this.from = from;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

