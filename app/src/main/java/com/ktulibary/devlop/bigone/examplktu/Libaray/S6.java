package com.ktulibary.devlop.bigone.examplktu.Libaray;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ktulibary.devlop.bigone.examplktu.R;

import java.util.Objects;

public class S6 extends AppCompatActivity {
    private android.support.v7.widget.Toolbar mtoolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s6);

        mtoolbar = findViewById(R.id.Regtool);
        setSupportActionBar(mtoolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("S6");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}